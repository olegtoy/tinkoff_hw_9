package ru.tinkoff.ru.seminar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by olegtojgildin on 27/11/2018.
 */

public class Forecast
{
    private List<Weather> list;
    public List<Weather> getList() {
        return list;
    }
    public void setList(List<Weather> fList) {
        this.list = fList;
    }
}
