package ru.tinkoff.ru.seminar;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.reactivex.Observable;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import ru.tinkoff.ru.seminar.model.Forecast;
import ru.tinkoff.ru.seminar.model.JsonWeatherDeserializer;
import ru.tinkoff.ru.seminar.model.Weather;

/**
 * Created by olegtojgildin on 25/11/2018.
 */

public class ApiServer implements WeatherServer {
    private static Api api;

    public ApiServer() {
        //Логирование
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        //OkHttpClient
        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(httpLoggingInterceptor)
                .addNetworkInterceptor(new StethoInterceptor())
                .build();

        //Gson парсер
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Weather.class, new JsonWeatherDeserializer());
        Gson gson = gsonBuilder.create();

        //Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .build();

        //Api server
        api = retrofit.create(Api.class);
    }

    @Override
    @GET("weather")
    public  Observable<Weather> getCurrentWeather(String city){
        return api.getCurrentWeather(city);
    };

    @Override
    @GET("forecast")

    public Observable<Forecast> getForecastWeather(String city){
        return api.getForecastWeather(city);

    };

    public  static Api getApi() {
        return api;
    }


}
