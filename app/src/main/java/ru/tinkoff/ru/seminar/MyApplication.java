package ru.tinkoff.ru.seminar;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by olegtojgildin on 26/11/2018.
 */

public class MyApplication extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
