package ru.tinkoff.ru.seminar;

/**
 * Created by olegtojgildin on 27/11/2018.
 */
import android.arch.lifecycle.LiveData;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionLiveData extends LiveData<ConnectionLiveData.ConnectionModel> {
    private Context context;
    ConnectionLiveData(Context context) {
        this.context = context;
    }
    @Override
    protected void onActive() {
        super.onActive();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        context.registerReceiver(networkReceiver, filter);
    }
    @Override
    protected void onInactive() {
        super.onInactive();
        context.unregisterReceiver(networkReceiver);
    }
    private BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @SuppressWarnings("deprecation")
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                NetworkInfo activeNetwork = (NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if (isConnected)
                {
                    postValue(new ConnectionModel(true));
                } else
                {
                    postValue(new ConnectionModel(false));
                }
            }
        }
    };

    class ConnectionModel {
        private boolean isConnected;
        boolean getIsConnected() {
            return isConnected;
        }
        ConnectionModel(boolean isConnected) {
            this.isConnected = isConnected;
        }
    }

}
