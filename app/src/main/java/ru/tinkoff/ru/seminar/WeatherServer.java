package ru.tinkoff.ru.seminar;

import java.util.List;
import java.util.WeakHashMap;
import io.reactivex.Observable;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.tinkoff.ru.seminar.model.Forecast;
import ru.tinkoff.ru.seminar.model.Weather;

/**
 * Created by olegtojgildin on 25/11/2018.
 */
@SuppressWarnings("SameParameterValue")
public interface WeatherServer {

    Observable<Weather> getCurrentWeather(String city);

    Observable<Forecast> getForecastWeather(String city);
}
