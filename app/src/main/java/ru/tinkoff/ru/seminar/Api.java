package ru.tinkoff.ru.seminar;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.tinkoff.ru.seminar.model.Forecast;
import ru.tinkoff.ru.seminar.model.Weather;

/**
 * Created by olegtojgildin on 25/11/2018.
 */

public interface Api {
  String params = "units=metric&lang=ru&appid=acf993bf91158e1b964db7d30554fc95";

    @GET("weather?"+params)
    Observable<Weather> getCurrentWeather(@Query("q") String city);

    @GET("forecast?"+params)
    Observable<Forecast> getForecastWeather(@Query("q") String city);

}
