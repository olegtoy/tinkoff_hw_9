package ru.tinkoff.ru.seminar.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by olegtojgildin on 25/11/2018.
 */

public class JsonWeatherDeserializer implements JsonDeserializer<Weather> {
    @Override
    public Weather deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)  {
        JsonObject responseJson = json.getAsJsonObject();
        long time = responseJson.get("dt").getAsLong() * 1000;

        String description = responseJson
                .get("weather")
                .getAsJsonArray()
                .get(0)
                .getAsJsonObject()
                .get("description")
                .getAsString();

        JsonObject mainJson = responseJson.getAsJsonObject("main");
        float temp = mainJson.get("temp").getAsFloat();

        JsonObject windJson = responseJson.getAsJsonObject("wind");
        float speedWind = windJson.get("speed").getAsFloat();

        return new Weather(description, time, temp, speedWind);
    }
}
